#coding: utf-8

from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from models import Category, Page, User, UserProfile
from forms import CategoryForm, PageForm, UserForm, UserProfileForm


def index(request):
    context_dict = {}

    category_list = Category.objects.order_by('-likes')[:5]
    context_dict['categories'] = category_list

    page_list = Page.objects.order_by('-views')[:5]
    context_dict['pages'] = page_list

    visits = request.session.get('visits','1')
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).days > 1:
            # ...reassign the value of the cookie to +1 of what it was before...
            visits = int(visits) + 1
            # ...and update the last visit cookie, too.
            reset_last_visit_time = True
    else:
        # Cookie last_visit doesn't exist, so create it to the current date/time.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits

    context_dict['visits'] = visits
    response = render(request,'rango/index.html', context_dict)

    return response


def about(request):
    context_dict = {}
    if request.session.get('visits'):
        visits = request.session.get('visits')
    else:
        visits = 0

    context_dict['visits'] = visits
    return render(request, 'rango/about.html', context_dict)

def category(request, category_name_slug):
    context_dict = {}
    try:
        category = Category.objects.get(slug=category_name_slug)
        category.views += 1
        category.save()
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages
        context_dict['category'] = category
    except Category.DoesNotExist:
        pass

    #context_dict['category_name'] = category_name_slug
    #context_dict['category'] = category_name_slug
    #print(category_name_slug)
    #return(HttpResponse(category_name_slug))
    return render(request, 'rango/category.html', context_dict)

@login_required
def add_category(request):
    context_dict = {}
    if request.method != "POST":
        form = CategoryForm()
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return index(request)

    context_dict['form'] = form
    return render(request, 'rango/add_category.html', context_dict)

@login_required
def add_page(request, category_name_slug):
    try:
        cat = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        cat = None

    if request.method != 'POST':
        form = PageForm()
    else:
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()
                return category(request, category_name_slug)
        else:
            print form.errors

    context_dict = {'form':form, 'category': cat}
    return render(request, 'rango/add_page.html', context_dict)

def register(request):
    context_dict = {}
    if request.user:
        logout(request)

    registered = False
    if request.method != "POST":
        user_form = UserForm()
        profile_form = UserProfileForm()
    else:
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            registered = True

    context_dict['registered'] = registered
    context_dict['user_form'] = user_form
    context_dict['profile_form'] = profile_form
    return render(request, 'rango/register.html', context_dict)

def user_login(request):
    context_dict = {}
    if request.method != "POST":
        return render(request, 'rango/login.html', context_dict)
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse("Sua conta está desabilitada.")
        else:
            erro = "Detalhes de login inválidos: Usuário: [{0}], Senha:[{1}]".format(username, password)
            print(erro)
            return HttpResponse(erro)

@login_required
def restricted(request):
    return render(request, 'rango/restricted.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')

def track_url(request):
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.save()
                return redirect(page.url)
            except:
                pass
    return redirect('/rango/')

def user_profile(request, username):
    context_dict={}

    u = None

    try:
        u = User.objects.get(username = username)
    except:
        return redirect('/rango/users_list/')

    try:
        user_profile = UserProfile.objects.get(user = u)
    except:
        user_profile = None

    context_dict['u'] = u
    context_dict['user_profile'] = user_profile
    return render(request, 'rango/user_profile.html', context_dict)

def users_list(request):
    users = User.objects.all()
    print(users)
    context_dict = {}
    context_dict['users'] = users
    return render(request, 'rango/users_list.html', context_dict)

def like_category(request):
    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET['category_id']

    likes = 0
    if cat_id:
        cat = Category.objects.get(id=int(cat_id))
        if cat:
            likes = cat.likes + 1
            cat.likes =  likes
            cat.save()

    return HttpResponse(likes)

def get_category_list(max_results=0, starts_with=''):
        cat_list = []
        if starts_with:
                cat_list = Category.objects.filter(name__istartswith=starts_with)

        if max_results > 0:
                if len(cat_list) > max_results:
                        cat_list = cat_list[:max_results]
        return cat_list

def suggest_category(request):
        context_dict = {}
        starts_with = ''
        if request.method == 'GET':
                starts_with = request.GET['suggestion']

        cats = get_category_list(8, starts_with)
        context_dict['cats'] = cats

        return render(request, 'rango/cats.html', context_dict)

# def add_category(request):
#     context_dict = {}
#
#     return render(request, 'rango/add_category.html', context_dict)